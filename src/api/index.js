const express = require('express');
const router = express.Router();
const regiserApi = require('../api/register');
const loginApi = require('../api/login');

router.use(regiserApi);
router.use(loginApi);

module.exports = router;
