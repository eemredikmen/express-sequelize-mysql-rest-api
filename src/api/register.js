const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.post('/register', async (req, res) => {
  const { fullName, email, password } = req.body;
  const alreadyExits = await User.findOne({ where: { fullName, email } }).catch(err => {
    console.log('Erorr' + err);
  })

  if (alreadyExits) {
    return res.status(400).json({
      message: 'User email already exits'
    })
  }

  const newUser = new User({ fullName, email, password });
  const savedUser = await newUser.save().catch(err => {
    console.log(err);
    res.status(404).json({
      message: 'Cannot register user at the momnet'
    });
  });

  if (savedUser) {
    res.status(200).json({
      message: 'User registration successful',
      data: {
        savedUser
      }
    })
  }
})

module.exports = router;

